LIST P=16F74
title "Temp Control"
__CONFIG B'11111110110010'

; *********************************************************************
; Case Study 4: Temperature Control
; Zhehao Mao and Mason Silber
; *********************************************************************

#include <P16F74.INC>

; Variable declarations
Ambient equ 20h
Feedback equ 21h
Setpoint equ 22h
Err equ 23h

org 00h
	goto initPort
org 04h
	goto isrService

; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

initPort
	movlw B'00000011' ; make sure fan and heater initially off
	movwf PORTD
	
	clrf Ambient
	clrf Feedback
	clrf Setpoint
	clrf Err

	; Set up ADCON0
	movlw B'01000001'
	movwf ADCON0
	
	bsf STATUS,RP0 ;Switch to bank 1

	; Set last 5 bits of PORTD to output, rest to input
	; pin 0 heater
	; pin 1 fan
	; pin 2 red LED
	; pin 3 yellow LED
	; pin 4 green LED
	; pin 5 switch
	movlw B'11100000'
	movwf TRISD

	movlw B'00000101'
	movwf ADCON1
	
	;Switch back to bank 0
	bcf STATUS,RP0

; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55
		
isSwitchOn
	btfsc PORTD,5
	goto takeAmbientReadingOne
	; make sure fan and heater are off when switch is off
	bsf PORTD,0
	bsf PORTD,1
	goto isSwitchOn

; &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	
takeAmbientReadingOne
	; clear bits 5-3 to choose RA0 as input
	bcf ADCON0,5
	bcf ADCON0,4
	bcf ADCON0,3
	bsf ADCON0,2     ; start conversion

waitForAmbientOne
	btfsc ADCON0,2   ; check if conversion is done
	goto waitForAmbientOne
	;Store A/D reading
	movf ADRES,W
	movwf Ambient

takeFeedbackReadingOne
	bsf ADCON0,3     ; set bit 3 to select RA1 as input
	bsf ADCON0,2     ; start conversion

waitForFeedbackOne
	btfsc ADCON0,2   ; check if conversion is done
	goto waitForFeedbackOne
	;Store A/D reading
	movf ADRES,W
	movwf Feedback

takeAmbientReadingTwo
	bcf ADCON0,3     ; clear bit 3 to select RA0 again
	bsf ADCON0,2     ; start conversion

waitForAmbientTwo
	btfsc ADCON0,2   ; check if conversion is done
	goto waitForAmbientTwo
	movf ADRES,W
	addwf Ambient,F  ; add reading to previous one
	rrf Ambient      ; divide by two to get average

takeFeedbackReadingTwo
	bsf ADCON0,3     ; set bit 3 to select RA1
	bsf ADCON0,2     ; start conversion

waitForFeedbackTwo
	btfsc ADCON0,2   ; check if conversion is finished
	goto waitForFeedbackTwo
	movf ADRES,W
	addwf Feedback,F ; add result to previous one
	rrf Feedback     ; divide by two to get average

; Ambient and Feedback are now our averages
; Calculate Setpoint Temperature
calculateSetpointTemp
	movf Ambient,W
	; change of 60 degrees C corresponds roughly to change of 
	; 57 on the reading from the Ambient sensor
	addlw .57 
	movwf Setpoint

getFeedbackInTermsOfAmbient
	; to get the feedback reading on the same scale as the ambient
	; we use the approximation ambient = feedback / 2 + 97
	bcf STATUS,C
	rrf Feedback,W
	addlw .97
	
calcError ; calculate error between Setpoint and Feedback
	subwf Setpoint,W
	movwf Err

checkSign ; check the sign of the error
	btfsc Err,7
	goto negativeCheck
	
positiveCheck
	; check whether or not error exceeds five degrees C
	; which corresponds to reading of +6 from the sensor
	sublw .6
	movwf Err
	btfss Err,7      ; if Err > 6, 6 - Err < 0
	goto turnOffHeaterAndFan
	goto turnOnHeater

negativeCheck
	; invert the number by complementing and adding 1
	comf Err,W
	incf Err,W
	sublw .6
	btfss Err,7      ; if Err > 6, 6 - Err < 0
	goto turnOffHeaterAndFan
	goto turnOnFan

turnOnHeater
	bsf PORTD,4      ; Green LED on
	bcf PORTD,3      ; Yellow LED off
	bcf PORTD,2      ; Red LED off
	bsf PORTD,1      ; fan off
	bcf PORTD,0      ; heater on
	goto isSwitchOn

turnOnFan
	bcf PORTD,4      ; Red LED off
	bcf PORTD,3      ; Yellow LED off
	bsf PORTD,2      ; Red LED on
	bcf PORTD,1      ; fan on
	bsf PORTD,0      ; heater off
	goto isSwitchOn

turnOffHeaterAndFan
	bcf PORTD,4      ; Green LED off
	bsf PORTD,3      ; Yellow LED on
	bcf PORTD,2      ; Red LED off
	bsf PORTD,1      ; fan off
	bsf PORTD,0      ; heater off
	goto isSwitchOn

; &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

isrService
	goto isrService

; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

END
