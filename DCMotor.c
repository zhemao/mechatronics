#include <pic.h>
__CONFIG(11111110110010);

#define PORTBIT(addr,bit) ((unsigned) (&addr) * 8 + (bit))

#define PWM_MAX 255
#define SUMERR_MAX ((1 << (sizeof(int) - 1)) - (1 << sizeof(char)))

#define PWEIGHT 10
#define DWEIGHT 1
#define IWEIGHTINV 10

#define SLOWVEL 20
#define BIAS 16

static bit blackButton @ PORTBIT(PORTA, 4);
static bit motorDir @ PORTBIT(PORTC, 0);
static bit motorBrake @ PORTBIT(PORTC, 1);
static bit eddyCurrent @ PORTBIT(PORTC, 3);
static bit encoder @ PORTBIT(PORTC, 7);

unsigned char nvm; /* number of velocity measurements */
unsigned char restvel;

unsigned int vavg;
unsigned int vtotal;
unsigned int vref;

int olderr; /* the last error */
int sumerr; /* sum of all the errors */

char mode;

void init(void)
{
	ADCON1 = 0x04;
	TRISA = 0xFF;
	TRISB = 0;
	TRISC = 0xF8;
	
	ADCON0 = 0b01001001;
	
	/* set up PWM */
	CCP1CON = 0b00001100;
	PR2 = 0xFF;
	T2CON = 0;
	
	/* zero out interrupt bits */
	PIE1 = 0;
	PIR1 = 0;
	PIE2 = 0;
	PIR2 = 0;
	
	/* set the bits for AD interrupt */
	GIE = 1;
	PEIE = 1;
	ADIE = 1;
	
	PORTA = 0;
	PORTB = 0;
	PORTC = 0;
	
	/* Initializing our class variables */
	vtotal = 0;
	vavg = 0;
	nvm = 0;
	vref = 0;
	olderr = 0;
	sumerr = 0;
}

void interrupt isrProcess(void)
{
	vtotal += ADRES;
	nvm++;
	
	if (nvm == 64) {
		vavg = vtotal >> 6;
		nvm = 0;
		vtotal = 0;
	}
	
	ADGO = 1;
	ADIF = 0;
}	

void set_pwm_out(int duty) {
	if (duty > PWM_MAX)
		CCPR1L = PWM_MAX;
	else if (duty < 0)
		CCPR1L = 0;
	else
		CCPR1L = duty;
}

/* Display error on console LEDs */
void display_error(void)
{
	int err;
	
	if (vref > vavg)
		err = (vref - vavg);
	else
		err = (vavg - vref);
		
	if (err > 255)
		PORTB = 255;
	else
		PORTB = err;
}

/* Mode 0 and 1 */
int pControl(char bias)
{
	return PWEIGHT * (vref - vavg) + bias;
}

/* Mode 2 */
int pdControl(void)
{
	char err = vref - vavg;
	int retval = PWEIGHT * err + DWEIGHT * (err - olderr);
	
	olderr = err;
	
	return retval;
}

/* Mode 3 */
int pidControl(void)
{
	int retval;
	char err = vref - vavg;
	
	sumerr += err;
	
	/* prevent sumerr from overflowing */
	if (sumerr > SUMERR_MAX)
		sumerr = SUMERR_MAX;
	else if (sumerr < -SUMERR_MAX)
		sumerr = - SUMERR_MAX;
	
	retval = PWEIGHT * err + DWEIGHT * (err - olderr) + sumerr / IWEIGHTINV;
	
	olderr = err;
	
	return retval;
}

/* Triggered when black button is pressed */
int control(void)
{
	switch (mode) {
		case 0:
			return pControl(0);
		case 1:
			return pControl(BIAS);
		case 2:
			return pdControl();
		case 3:
			return pidControl();
	}	
}	

void delay(int cycles)
{
	int i;
	
	for (i = 0; i < cycles; i++);
}

void flash(void) {
	PORTB = 0xFF;
	delay(1000);
	PORTB = 0;
}

/* Main control method, controls the trapezoidal velocity
   profile */
void controlMotor(void)
{
	char encoderHits = 0;
	char revolutions = 0;
	
	vref = restvel + SLOWVEL;
	sumerr = 0;
	
	/* Increasing portion of trapezoidal velocity profile */
	while (vref < 190) {
		if (encoder) {
			encoder = 0;
			encoderHits++;
			
			if (encoderHits == 100) {
				vref++;
				encoderHits = 0;
			}
		}
		
		display_error();
		set_pwm_out(control());
	}
	
	encoderHits = 0;
	
	/* Constant portion of trapezoidal velocity profile (max velocity) */
	while (revolutions < 60) {
		if (encoder) {
			encoder = 0;
			encoderHits++;
			
			if (encoderHits == 100) {
				revolutions++;
				encoderHits = 0;
			}	
		}
		
		display_error();
		set_pwm_out(control());
	}	
	
	encoderHits = 0;
	
	/* Decreasing portion of the trapezoidal velocity profile */
	while (vref > 140) {
		if (encoder) {
			encoder = 0;
			encoderHits++;
			
			if (encoderHits == 100) {
				vref--;
				encoderHits = 0;
			}
		}
		
		display_error();
		set_pwm_out(control());
	}
	
	set_pwm_out(0);
	motorBrake = 1;
	
	while (vavg > restvel);
	
	motorBrake = 0;
	
	flash();
}

/* Calibration for the tachometer, run when system is powered on */
void initialSpin(void)
{
	char sensorHits = 0;
	char sensorOn = 0;
	
	ADGO = 1;
	TMR2ON = 1;
	motorDir = 1;
	
	/* wait for the initial readings from the tachometer */
	while (vavg == 0);
	
	restvel = vavg;
	vref = restvel + SLOWVEL;
	 
	while (sensorHits < 4) {
		if (eddyCurrent && !sensorOn) {
			sensorOn = 1;
			sensorHits++;
		} else if (!eddyCurrent && sensorOn)
			sensorOn = 0;
		
		set_pwm_out(pControl(0));
	}
	
	set_pwm_out(0);
	motorBrake = 1;
	
	while (vavg != restvel);
	
	motorBrake = 0;
	PORTB = restvel;
}

void fault(void)
{
	while (1) {
		flash();
		delay(1000);
	}	
}	

void main(void)
{
	init();
	
	initialSpin();
	
	while (1) {
		while (!blackButton);
		while (blackButton);
		
		/* read the hex switch value */
		mode = (PORTC >> 4) & 0x3;
		
		if (mode > 3)
			fault();
		
		controlMotor();
	}
}