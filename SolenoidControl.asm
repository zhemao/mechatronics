LIST P=16F74
title "Solenoid"
__CONFIG B'11111110110010'

#include <P16F74.INC>

;Variable Declarations
Main 	equ 0
Reduced equ 1
Sensor 	equ 2
State 	equ 20h
Count 	equ 21h
Timer2	equ 22h
Timer1	equ 23h
Timer0	equ 24h

org 00h	
	goto initPort
	
org 04h
	goto isrService
	
org 05h

;Port Initialization
initPort
	;Clear the ports we're using
	clrf	PORTB
	clrf	PORTC
	clrf 	PORTD
	clrf	PORTE
	
	; Set up ADCON0
	movlw B'01000001'
	movwf ADCON0
	
	;Switch to bank 1
	bsf		STATUS,RP0
	
	;Set lower half of PORTB to output, rest to input
	movlw	B'11110000'
	movwf	TRISB
	
	; Set last two bits and top two bits of PORTD to output, 
	; rest to input
	movlw	B'11111100'
	movwf	TRISD
	
	;Set TRISC and TRISE to all inputs
	movlw	B'11111111'
	movwf	TRISC
	
	movlw	B'00000111'
	movwf	TRISE
	
	; Configure ADCON1 for digital input on PORTE
	movlw	B'00000100'
	movwf	ADCON1
	
	;Switch back to bank 0
	bcf		STATUS,RP0
	
;Wait for green button to be pressed
WaitGreenPressed
	btfss	PORTC,0
	goto	WaitGreenPressed
	
;Wait for green button to be released
WaitGreenReleased
	btfsc	PORTC,0
	goto	WaitGreenReleased

;Setting the mode of the solenoid
SetMode
	;Send complement of octal switch to our state variable
	comf	PORTE,W
	movwf	State
	
	;Check if highest bit is zero (as it should be)
	;If it's not, we're in an invalid mode, so we handle
	;appropriately
	btfsc	State,2
	goto	Fault
	
	;If the upper bit is set, check if it's three or two (in ThreeOrTwo)
	;If not, check if it's one or zero in OneOrZero
	btfss	State,1
	goto	OneOrZero
	
;Check if a number is three or two
ThreeOrTwo
	btfss	State,0
	;It is two
	goto	ModeTwo
	;It is three
	goto	ModeThree
	
;Check if a number is one or zero
OneOrZero
	btfss	State,0
	;It is zero
	goto	Fault
	;It is one
	goto	ModeOne
	
;Label for mode one
ModeOne
	movlw	B'00000001'
	movwf	PORTB

; Wait for red or green button to be pressed	
ModeOneWaitPressed
	btfsc	PORTC,1
	; if red pressed, continue
	goto	ModeOneWaitReleased 
	btfsc	PORTC,0
	; if green pressed, back to mode selection
	goto	WaitGreenReleased 
	; if neither pressed, keep checking
	goto	ModeOneWaitPressed
	
;Wait for the red button to be released
ModeOneWaitReleased
	btfsc	PORTC,1
	goto	ModeOneWaitReleased
	
ToggleTransistor
	;Toggle the main transistor
	btfss	PORTD,0
	goto ModeOneSetTransistor
	goto ModeOneClearTransistor

; Set main transistor to high
ModeOneSetTransistor
	bsf		PORTD,0
	goto ModeOneWaitPressed

; Set main transistor to low
ModeOneClearTransistor
	bcf		PORTD,0
	; Go back to waiting for button press
	goto ModeOneWaitPressed
	
;Label for mode two
ModeTwo
	movlw	B'00000010'
	movwf	PORTB

ModeTwoWaitPressed
	bcf	PORTD,0 ; make sure transistor turned off
	btfsc	PORTC,0 ; check if green button pressed
	goto	WaitGreenReleased ; if pressed, back to mode selection
	btfss	PORTC,1 ; check if red button pressed
	goto	ModeTwoWaitPressed ; if not pressed, go back to waiting

ModeTwoWaitReleased
	btfsc	PORTC,1 ; wait for red button to be released
	goto	ModeTwoWaitReleased
	bsf	ADCON0,GO ; when released, start AD converter
	
ModeTwoWaitADValue
	btfsc	ADCON0,GO ; wait for AD converter to finish
	goto	ModeTwoWaitADValue
	; move AD converter value into Count
	movf	ADRES,W
	movwf	Count
	; check if AD Value is 0
	addlw	0h
	btfsc	STATUS,Z
	goto	Fault ; if it is, then go to Fault
	
ModeTwoSetupDelay
	; rotate right by 2 (divide by 4)
	bcf	STATUS,C
	rrf	Count,F
	bcf	STATUS,C
	rrf	Count,F
	bsf	PORTD,0 ; engage the main transistor

ModeTwoSecondDelay ; set up an approximately 1-second delay
	movlw 05h
	movwf Timer2
	movlw 16h
	movwf Timer1
	movlw 15h
	movwf Timer0

ModeTwoDelay
	btfsc	PORTC,1 ; check if red button has been pressed again
	goto	ModeTwoWaitReleased ; if so, reset the count
	decfsz Timer0,F
	goto ModeTwoDelay
	decfsz Timer1,F
	goto ModeTwoDelay
	decfsz Timer2,F
	goto ModeTwoDelay
	
	decfsz	Count ; decrement count and check if it's zero
	goto 	ModeTwoSecondDelay ; if it's zero, wait another second
	goto 	ModeTwoWaitPressed ; otherwise, go back to beginning of mode

;Label for mode three
ModeThree
	movlw	B'00000011'
	movwf	PORTB
	
ModeThreeWaitPressed
	btfsc	PORTC,0
	; if green pressed, back to mode selection
	goto	WaitGreenReleased 
	btfss	PORTC,1 ; wait for red button to be pressed
	goto	ModeThreeWaitPressed

ModeThreeWaitReleased
	btfsc	PORTC,1 ; wait for red button to be released
	goto	ModeThreeWaitReleased
	bsf	ADCON0,GO ; start the AD converter
	
ModeThreeWaitADValue
	btfsc	ADCON0,GO ; wait for AD converter to finish
	goto	ModeThreeWaitADValue
	movf	ADRES,W ; move the AD result to count
	movwf	Count
	; check if ADValue is 0
	addlw	0h
	btfsc	STATUS,Z
	goto	Fault

ModeThreeSetupDelay
	; rotate right by 2 (divide by 4)
	bcf	STATUS,C
	rrf	Count,F
	bcf	STATUS,C
	rrf	Count,F

EngageSolenoid
	call	SetupCounter ; set up the counter
	bsf	PORTD,0 ; engage the main solenoid

WaitForEngage
	btfsc	PORTD,2 ; check if solenoid is engaged
	goto	SwitchToReduced ; if it is, switch to reduced transistor
	decfsz	Timer0,F
	goto	WaitForEngage
	decfsz	Timer1,F
	goto	WaitForEngage
	decfsz	Timer2,F
	goto	WaitForEngage
	goto	Fault ; if time runs out, go to Fault

SwitchToReduced
	bsf	PORTD,1 ; turn on reduced
	bcf	PORTD,0 ; turn off main
	
ModeThreeSecondDelay ; set up approx. one second delay
	movlw 06h
	movwf Timer2
	movlw 16h
	movwf Timer1
	movlw 15h
	movwf Timer0

ModeThreeDelay
	decfsz Timer0,F
	goto ModeThreeDelay
	decfsz Timer1,F
	goto ModeThreeDelay
	decfsz Timer2,F
	goto ModeThreeDelay
	
	decfsz	Count
	goto 	ModeThreeSecondDelay ; if count is not zero, delay another second
	
DisengageSolenoid
	bcf	PORTD,1 ; turn off the reduced transistor
	call	SetupCounter
WaitForDisengage
	btfss	PORTD,2 ; check if solenoid has disengaged
	goto	ModeThreeWaitPressed
	decfsz	Timer0,F
	goto	WaitForDisengage
	decfsz	Timer1,F
	goto	WaitForDisengage
	decfsz	Timer2,F
	goto	WaitForDisengage
	goto	Fault ; if time runs out, go to Fault

SetupCounter ; set up a ten-second countdown
	movlw	23h
	movwf	Timer2
	movlw	0CDh
	movwf	Timer1
	movlw	30h
	movwf	Timer0
	return

Fault
	btfss	PORTB,3
	goto	SetFault
	clrf	PORTB
	goto	FaultHalfSecondDelay

SetFault
	comf	PORTE,W
	movwf	PORTB
	bsf	PORTB,3
	
FaultHalfSecondDelay
	movlw 03h
	movwf Timer2
	movlw 0Bh
	movwf Timer1
	movlw 0Ah
	movwf Timer0

FaultDelay
	decfsz Timer0,F
	goto FaultDelay
	decfsz Timer1,F
	goto FaultDelay
	decfsz Timer2,F
	goto FaultDelay
	
	goto Fault

isrService
	goto isrService

END