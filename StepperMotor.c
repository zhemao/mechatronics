#include <pic.h>
__CONFIG(11111110110010);

#define STEP_PERIOD 3846

#define PORTBIT(adr,bit) ((unsigned)(&adr)*8+(bit))

/* Global variables */

/* Buttons */
static bit greenButton	@	PORTBIT(PORTC,0);
static bit redButton	@	PORTBIT(PORTC,1);

/* Stepper motor sensors */
static bit uniHoriz @ PORTBIT(PORTB, 4);
static bit uniVert  @ PORTBIT(PORTB, 5);
static bit biVert  @ PORTBIT(PORTB, 6);
static bit biHoriz   @ PORTBIT(PORTB, 7);

static char uniPin;
static char biPin;

/* What stage the motors are in at any point in time. Used in all modes to keep
 * track of which direction the motors are going. */
static char stage;

/* Initialization of global variables */
void init(void)
{
	uniPin = 0;
	biPin = 0;
	stage = 0;
	
	PORTB = 0;
	PORTD = 0x53; /* stepper motors at their initial settings */
	
	/* Setting ports to input and output */
	TRISB = 0xF0;
	TRISC = 0xFF;
	TRISD = 0x00;
	TRISE = 0x07;
	ADCON1 = 0x02;
}

void delay(unsigned long ticks)
{
	unsigned long i;
	for (i=0; i<ticks; i++);
}

/* Display the mode on the port B LEDs */
void display_mode(char mode)
{
	PORTB &= 0xF0;
	PORTB |= mode;
}

/* When fault occurs, display the fault mode on port B and then never leave this
 * mode */
void fault(void)
{
	PORTB |= (1 << 3);
	while(1);
}

/* Used to switch magnets to turn bipolar stepper motor on and off in the
 * correct sequence 
 * direction: -1 for CCW, 1 for CW */
void tick_bipolar(char direction)
{
	biPin = (biPin + direction) % 4;
	PORTD &= 0x0F;
	
	/* Toggling magnets on and off in sequence */
	switch(biPin) {
		case 0:
			PORTD |= 0x50; /* 4 high 6 high */
			break;
		case 1:
			PORTD |= 0x40; /* 6 high 4 low */
			break;
		case 2:
			PORTD |= 0x00; /* 4 low 6 low */
			break;
		case 3:
			PORTD |= 0x10; /* 6 low 4 high */
			break;
	}	
}

/* Turn the bipolar stepper motor
 * direction: -1 for CCW, 1 for CW */
void turn_bipolar(char direction)
{
	/* Nested while loops below move motors until sensor is triggered */

	/* Clockwise */
	if (direction == 1) {
		while (!biVert) {
			tick_bipolar(direction);
			delay(STEP_PERIOD);
		}	
	}
	/* Counterclockwise */
	else {
		while (!biHoriz) {
			tick_bipolar(direction);
			delay(STEP_PERIOD);
		}	
	}	
}	

/* Used to turn the magnets of the unipolar stepper motor in sequence
 * direction: -1 for CCW, 1 for CW */
void tick_unipolar(char direction)
{
	uniPin = (uniPin + direction) % 4;
	PORTD &= 0xF0;
	PORTD |= (1 << uniPin);
	PORTD |= (1 << ((uniPin + 1) % 4));
}	

/* Turns the unipolar step motor 
 * direction: -1 for CCW, 1 for CW */
void turn_unipolar(char direction)
{	
	/* Nested while loops below move motors until sensor is triggered */

	/* Clockwise */
	if (direction == 1) {
		while (!uniHoriz) {
			tick_unipolar(direction);
			delay(STEP_PERIOD);
		}
	}
	/* Counterclockwise */
	else {
		while (!uniVert) {
			tick_unipolar(direction);
			delay(STEP_PERIOD);
		}
	}	
}

/* Mode 1: motors alternate moving based on sensor output */
void mode1(void) {
	
	/* Call the correct motor to move in correct direction in sequence */
	switch (stage) {
		case 0:
			turn_unipolar(-1);
			break;
		case 1:
			turn_bipolar(1);
			break;
		case 2:
			turn_unipolar(1);
			break;
		case 3:
			turn_bipolar(-1);
			break;
	}
	
	stage = (stage + 1) % 4;
}

/* Mode 2: motors run in opposite directions */
void turn_opposing(void)
{
	/* One stage of mode 2, where unipolar stepper motor turns CCW and
	 * bipolar stepper motor turns CW */
	if (stage == 0) {
		while (!uniVert || !biVert) {
			if (!uniVert)
				tick_unipolar(-1);
			
			if (!biVert)
				tick_bipolar(1);
			
			delay(STEP_PERIOD);
		}
	}
	/* Other stage of mode 2, where unipolar stepper motor turns CW and
	 * bipolar stepper motor turns CCW */
	else {
		while (!uniHoriz || !biHoriz) {
			if (!uniHoriz)
				tick_unipolar(1);
				
			if (!biHoriz)
				tick_bipolar(-1);
			
			delay(STEP_PERIOD);
		}	
	}
}

/* Mode 3: motors run in the same direction */
void turn_together(void)
{
	
	/* Stage 0 of mode 3, turn both clockwise */
	if (stage == 0) {
		while (!uniHoriz || !biVert) {
			if (!uniHoriz)
				tick_unipolar(1);
			
			if (!biVert)
				tick_bipolar(1);
			
			delay(STEP_PERIOD);
		}
	} 
	/* Turn both counterclockwise */
	else {
		while (!uniVert || !biHoriz) {
			if (!uniVert)
				tick_unipolar(-1);
				
			if (!biHoriz)
				tick_bipolar(-1);
			
			delay(STEP_PERIOD);
		}	
	}
}

/* Function that handles either mode 2 or mode 3, based on the mode passed in 
 * as a parameter. Relatively straightforward */
void mode23(char mode) {

	
	while (!redButton) {
		if (mode == 2)
			turn_opposing();
		else
			turn_together();
		stage = !stage;
	}
	
	/* wait for red to be released */
	while (redButton);
}

/* Main function */
void main(void)
{
	/* Variable to keep track of mode */
	char mode = 0;
	
	init();
	
	while (1) {
		
		/* If green button is pressed */
		if (greenButton) {
			/* Wait until green button is released */
			while (greenButton);
			mode = ~PORTE & 0x07;
			/* Display the mode on port B LEDs */
			display_mode(mode);
			/* Check for valid mode */
			if (mode < 1 || mode > 3)
				fault();
			stage = 0;
		}
		/* if red button is pressed */
		else if (redButton) {
			while (redButton);
			
			/* Call correct function based on mode */
			if (mode == 1)
				mode1();
			/* Pass mode value into the function, which will handle
			 * it appropriately */
			else
				mode23(mode);
		}
	}
}
